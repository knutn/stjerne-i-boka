package no.stjerneiboka.helloworld


data class Greeting(val id: Int, val greeting: String)