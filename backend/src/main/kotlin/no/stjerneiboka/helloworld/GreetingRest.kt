package no.stjerneiboka.helloworld

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class GreetingRest(private val greetingService: GreetingService) {
    @GetMapping("hello")
    fun hello(): Greeting {
        return greetingService.randomGreeting()
    }
}
