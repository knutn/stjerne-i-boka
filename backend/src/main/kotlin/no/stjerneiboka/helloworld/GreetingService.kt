package no.stjerneiboka.helloworld

import org.springframework.stereotype.Service

@Service
class GreetingService(private val greetingDao: GreetingDao) {
    fun randomGreeting(): Greeting {
        val greetings = greetingDao.all()
        return greetings[Math.floor(Math.random() * greetings.size).toInt()]
    }
}
