package no.stjerneiboka.auth

import com.auth0.jwk.UrlJwkProvider
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.jwt.JwtHelper
import org.springframework.security.jwt.crypto.sign.RsaVerifier
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.stereotype.Component
import java.net.URL
import java.security.interfaces.RSAPublicKey
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class GoogleOpenIdConnectFilter(private val objectMapper: ObjectMapper,
                                private val restTemplate: OAuth2RestTemplate,
                                @Value("\${google.client-id}") private val clientId: String,
                                @Value("\${google.issuer}") private val issuer: String,
                                @Value("\${google.jwk-url}") private val jwkUrl: String) :
        AbstractAuthenticationProcessingFilter("/api/auth/code") {

    init {
        authenticationManager = AuthenticationManager {
            throw UnsupportedOperationException("No authentication should be done with this AuthenticationManager")
        }
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        try {
            val accessToken = restTemplate.accessToken
            val idToken = accessToken.additionalInformation["id_token"].toString()
            val kid = JwtHelper.headers(idToken)["kid"]!!
            val tokenDecoded = JwtHelper.decodeAndVerify(idToken, verifier(kid))
            val authInfo = objectMapper.readValue(tokenDecoded.claims, Map::class.java)
            verifyClaims(authInfo)
            val user = object : UserDetails {
                override fun getAuthorities() = listOf(SimpleGrantedAuthority("ROLE_USER"))
                override fun isEnabled() = true
                override fun getUsername() = authInfo["email"] as String
                override fun isCredentialsNonExpired() = true
                override fun getPassword() = null
                override fun isAccountNonExpired() = true
                override fun isAccountNonLocked() = true

            }
            return UsernamePasswordAuthenticationToken(user, null, user.authorities)
        } catch (e: Exception) {
            throw BadCredentialsException("Could not obtain user details from token", e)
        } catch (e: OAuth2Exception) {
            throw BadCredentialsException("Could not obtain access token", e)
        }

    }

    private fun verifyClaims(claims: Map<*, *>) {
        val exp = claims["exp"] as Int
        val expireDate = Date(exp * 1000L)
        val now = Date()
        if (expireDate.before(now) || claims["iss"] != issuer || claims["aud"] != clientId) {
            throw RuntimeException("Invalid claims")
        }
    }

    private fun verifier(kid: String): RsaVerifier {
        val provider = UrlJwkProvider(URL(jwkUrl))
        val jwk = provider.get(kid)
        return RsaVerifier(jwk.publicKey as RSAPublicKey)
    }
}