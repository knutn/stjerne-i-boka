package no.stjerneiboka.auth

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.oauth2.client.OAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client

@Configuration
@EnableOAuth2Client
class GoogleOpenIdConnectConfig {
    @Bean
    fun openId(
            @Value("\${google.client-id}") clientId: String,
            @Value("\${google.client-secret}") clientSecret: String,
            @Value("\${google.access-token-uri}") accessTokenUri: String,
            @Value("\${google.user-authorization-uri}") userAuthorizationUri: String,
            @Value("\${google.redirect-uri}") redirectUri: String): OAuth2ProtectedResourceDetails {
        val details = AuthorizationCodeResourceDetails()
        details.clientId = clientId
        details.clientSecret = clientSecret
        details.accessTokenUri = accessTokenUri
        details.userAuthorizationUri = userAuthorizationUri
        details.scope = listOf("email")
        details.preEstablishedRedirectUri = redirectUri
        details.isUseCurrentUri = false
        return details
    }

    @Bean
    fun googleOpenIdTemplate(clientContext: OAuth2ClientContext,
                             details: OAuth2ProtectedResourceDetails): OAuth2RestTemplate {
        return OAuth2RestTemplate(details, clientContext)
    }
}