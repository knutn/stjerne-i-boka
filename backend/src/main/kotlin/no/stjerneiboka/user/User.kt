package no.neksa.sqf.booking.user

class User(
        val id: Int,
        val email: String,
        val name: String
)
