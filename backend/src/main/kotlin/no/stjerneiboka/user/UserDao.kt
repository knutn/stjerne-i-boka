package no.stjerneiboka.user

import no.neksa.sqf.booking.user.User
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Component

@Component
class UserDao(val namedTemplate: NamedParameterJdbcTemplate) {
    val userRowMapper = RowMapper { rs, _ ->
        User(rs.getInt("id"), rs.getString("email"), rs.getString("name"))
    }

    fun findById(id: Int): User {
        return namedTemplate.queryForObject(
                "SELECT * FROM user WHERE id = :id",
                MapSqlParameterSource().addValue("id", id),
                userRowMapper)!!
    }

    fun findByEmail(email: String): User {
        return namedTemplate.queryForObject(
                "SELECT * FROM user WHERE email = :email",
                MapSqlParameterSource().addValue("email", email),
                userRowMapper)!!
    }
}